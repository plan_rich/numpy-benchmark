#!/usr/bin/env python
from __future__ import print_function, division

# this script requires the following packages on pypi: perf, py

import numpy
import os
import py
import sys
import csv
import datetime
import argparse
import perf.text_runner
import platform

arraylib = "numpy"

setup_base = """
    import {arraylib} as np

    N = {N}

    a=np.random.rand({N})
    b=np.random.rand({N})
    out=np.random.rand({N})

    M    = np.random.rand({N} * {N}).reshape(({N}, {N}))
"""

def create_setup(string, **kwargs):
    return string.format(**kwargs)

benchmarks = [
    ('a+=42.24', 8000),
    ('a*=b', 8000),
    ('M.dot(a)', 5000),
    ('M.copy()', 2250),
    ('M.sum()',5000),
]


microbench_str = """\
def microbench(loops):
    # setup
    {setup}
    t0 = perf.perf_counter()
    # exec micro bench
    import gc
    gc.collect()
    for l in range(loops):
        {stmt}
    #
    e0 = perf.perf_counter()
    gc.collect()
    return e0 - t0
"""

def perf_benchmark(argparser, N, stmt, vec):
    def prepare(runner, args):
        args.append("--stmt")
        args.append(stmt)
        args.append("--N")
        args.append(N)
        # addition for pypy
        if vec != "":
            args.insert(1, "--jit")
            args.insert(2, "vec="+vec)
        print(' '.join(args))
    name = stmt
    setup = create_setup(setup_base, arraylib=arraylib, N=N)
    benchcode = py.code.Source(microbench_str.format(**locals())).compile()
    exec benchcode in globals(), locals()

    runner = perf.text_runner.TextRunner(name=name, inner_loops=100,
                                         _argparser=argparser)
    runner.prepare_subprocess_args = prepare
    return runner.bench_sample_func(microbench)

def build_argparser():
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--vec", dest="vec", default="")
    argparser.add_argument("--stmt", dest="stmt")
    argparser.add_argument("--N", dest="N")
    argparser.add_argument("--interp", dest="interp")
    return argparser

if __name__ == '__main__':
    try:
        sys.argv.index("--worker")
        worker = True
    except ValueError:
        worker = False

    if not worker:
        print("running %d benchmarks...\n" % len(benchmarks))
        for i,(stmt,N) in enumerate(benchmarks):
            small = False
            argparser = build_argparser()
            args, _ = argparser.parse_known_args()
            res = perf_benchmark(argparser, str(N), stmt, args.vec)
            fmt = "['%s','%s',%g,%g,'%s',%d,%d,%d,%d,%s]"
            arch, interp = args.interp.split('-')
            name = res.get_name()
            bench = stmt + ' N=' + str(N)
            loops = res.get_nsample()
            warmup = res._get_nwarmup()
            samples = res._get_nsample_per_run()
            us = numpy.median(res.get_samples()) / 1000.0
            std = numpy.std(res.get_samples())
            pythonliststr = fmt % (interp, arch, us, std, bench, N, loops, warmup, samples, str(small))
            sys.stderr.write(pythonliststr + '\n')
    else:
        argparser = build_argparser()
        args, _ = argparser.parse_known_args()
        perf_benchmark(argparser, args.N, args.stmt, args.vec)
