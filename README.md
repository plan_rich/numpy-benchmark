Micro-benchmarks for some numpy functions
=========================================

Forked version. Run it in the virtual env you want to test:

```
$ python benchmark.py --samples 5 --warmups 3 --loops 100 --N 100 2> <arch>.txt
```

Copy the generated lines in the jupyter notebook and see the results.
